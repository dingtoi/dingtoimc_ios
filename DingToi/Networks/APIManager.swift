//  Created by boys vip on 6/18/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON
import Alamofire

//let param:[String : Any]! = [
//    "UserName": request.UserName ?? "",
//    "Password": request.Password ?? ""
//]

//if let object = ResponseRegisterModel.deserialize(from: success) {
//    delegate.OnSuccess_Register(result: object)
//}else{
//    let err = ResponseErrorModel.init(status: HttpsRequest.CODE, message: self.err_Default, errors: nil)
//    delegate.OnError_Register(error: err)
//}

public final class APIManager {
    public static let shared = APIManager()
    private let checkReportError = true
    public static let url = "http://dingtoi.com:30080/api/v1"
    let err_Default = "Máy chủ đang bận, vui lòng thử lại sau !"
    
    let urlTransactionCheck = "\(APIManager.url)/transaction/check"
    let urlTextInboundCheck = "\(APIManager.url)/transaction/text/check/inbound"
    let urlVoiceInboundCheck = "\(APIManager.url)/transaction/voice/check/inbound"
    
    // Check Transaction Code
    func checkTransactionCode(transactionCode: String, delegate: TransactionCodeDelegate) {
        
        HttpsRequest.init().GET("\(urlTransactionCheck)/\(transactionCode)", nil, nil, { (response, success) -> (Void) in
            if let result = ResponeModel.deserialize(from: success) {
                delegate.onSuccessTransactionCode(response: result)
            } else {
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorTransactionCode(error: err)
            }
        }, {(response, error) -> (Void) in
            if let err = ResponseErrorModel.deserialize(from: error) {
                delegate.onErrorTransactionCode(error: err)
            }else{
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorTransactionCode(error: err)
            }
        })
    }
    
    // Check Text Inbound
    func checkTextInbound(phoneNumber: String, delegate: InboundDelegate) {
        let params: [String: Any] = [
            "phone": phoneNumber
        ]
        HttpsRequest.init().POST(urlTextInboundCheck, params, nil, { (response, success) -> (Void) in
            if let result = ResponeModel.deserialize(from: success) {
                delegate.onSuccessTextInbound(response: result)
            } else {
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorInbound(error: err)
            }
        }, {(response, error) -> (Void) in
            if let err = ResponseErrorModel.deserialize(from: error) {
                delegate.onErrorInbound(error: err)
            }else{
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorInbound(error: err)
            }
        })
    }
    
    // Check Voice Inbound
    func checkVoiceInbound(phoneNumber: String, delegate: InboundDelegate) {
        let params = [
            "phone": phoneNumber
        ]
        
        HttpsRequest.init().POST(urlVoiceInboundCheck, params, nil, { (response, success) -> (Void) in
            if let result = ResponeModel.deserialize(from: success) {
                delegate.onSuccessVoiceInbound(response: result)
            } else {
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorInbound(error: err)
            }
        }, {(response, error) -> (Void) in
            if let err = ResponseErrorModel.deserialize(from: error) {
                delegate.onErrorInbound(error: err)
            }else{
                print(">>>  Exception Pase Json -------------------------")
                let err = ResponseErrorModel.init(status: HttpsRequest.CODE_DEFAULT, message: self.err_Default, errors: nil)
                delegate.onErrorInbound(error: err)
            }
        })
    }
}
