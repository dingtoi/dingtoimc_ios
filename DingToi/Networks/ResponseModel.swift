//
//  ResponseModel.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/22/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import HandyJSON

public class ResponeModel: HandyJSON {
    var isSuccess: Bool
    var response: NSObject?
    
    required public init() {
        self.isSuccess = false
    }
    
    public init(isSuccess: Bool, response: NSObject?) {
        self.isSuccess = isSuccess
        self.response = response
    }
}
