//  Created by boys vip on 6/23/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation
import HandyJSON

public class ResponseDataModel: HandyJSON {
    var status: String?
    var data: Dictionary<String,String>?
    var message: String?
    var errors: Dictionary<String,String>?
    
    init(status: String?, data: Dictionary<String,String>?, message: String?, errors: Dictionary<String,String>?) {
        self.status = status
        self.data = data
        self.message = message
        self.errors = errors
    }
    public required init() {}
}
