//
//  MainVC.swift
//  TemplaceProject
//
//  Created by Tran Viet Thuc on 1/6/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation

class ScannerViewController: BaseViewController {
    @IBOutlet weak var _view: ScannerDetailView! {
        didSet {
            isBottomSafeArea = false
            baseView = _view
            self._view.rootVC = self
            self._view.hanlder = self.handler
        }
    }
    var handler: ScannerHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cbManager.delegate = self
        self.methodStatusDelegate = self._view
    }
    
    var cbManager: CBCentralManager = CBCentralManager()
    weak var methodStatusDelegate: MethodStatusDelegate?
    
    deinit {
        print("deinit called")
    }
}

extension ScannerViewController: CBCentralManagerDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("didFailToConnect")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnect")
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if #available(iOS 10.0, *) {
            self.methodStatusDelegate?.bluetoothScanned(status: central.state)
        } else {
            
        }
    }
}
