//
//  LableCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class LableCell: UITableViewCell {

    @IBOutlet weak var _lbKey: UILabel!
    @IBOutlet weak var _lbValue: UILabel!
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _viewContent: UIView!
    @IBOutlet weak var _imgStatus: UIImageView!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self._viewContent.layer.cornerRadius = 10
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(infoCell: InfoCell) {
        self._lbKey.text = infoCell.key
        self._lbKey.sizeToFit()
        self._lbValue.text = infoCell.value
        self._lbValue.sizeToFit()
        _img.image = UIImage(named: infoCell.imageName ?? "")
        if let isFailed = infoCell.isFailed, isFailed {
            self._imgShadow.image = UIImage(named: "bg_not_working_1")
            
            self._imgStatus.image = UIImage(named: "failed")
        } else {
            self._imgShadow.image = UIImage(named: "bg_working_1")
            self._imgShadow.isHidden = false
            self._imgStatus.image = UIImage(named: "checked")
        }
        self._lbKey.frame.y = self._img.frame.midY - self._lbKey.frame.height
        self._lbValue.frame.height = self._lbKey.frame.height
        self._lbValue.frame.y = self._img.frame.midY
        self._imgStatus.frame.origin.x = self._lbKey.frame.maxX + 10
        self._imgStatus.frame.midY = self._lbKey.frame.midY
    }
    
    static var className: String {
        return String(describing: self)
    }
}
