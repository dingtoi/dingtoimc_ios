//
//  HardwareCell.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/6/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class HardwareCell: UITableViewCell {
    @IBOutlet weak var _viewContent: UIView!
    @IBOutlet weak var _img: UIImageView!
    @IBOutlet weak var _lbKey: UILabel!
    @IBOutlet weak var _imgCheckAll: UIImageView!
    @IBOutlet weak var _lbKey1: UILabel!
    @IBOutlet weak var _lbValue1: UILabel!
    @IBOutlet weak var _lbKey2: UILabel!
    @IBOutlet weak var _lbValue2: UILabel!
    @IBOutlet weak var _lbKey3: UILabel!
    @IBOutlet weak var _lbValue3: UILabel!
    @IBOutlet weak var _lbLine1: UILabel!
    @IBOutlet weak var _lbLine2: UILabel!
    @IBOutlet weak var _lbLine3: UILabel!
    @IBOutlet weak var _viewShadow: UIView!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self._viewContent.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(infoCell: InfoCell) {
        self._lbKey.text = infoCell.key
        self._lbKey.sizeToFit()
        self._lbKey.frame.midY = self._img.frame.midY
        self._imgCheckAll.frame.x = self._lbKey.frame.maxX + 10
        self._imgCheckAll.frame.midY = self._lbKey.frame.midY
        self._lbKey1.text = infoCell.items?.key1
        
        self._lbValue1.text = infoCell.items?.value1
        self._lbKey2.text = infoCell.items?.key2
        self._lbValue2.text = infoCell.items?.value2
        self._lbKey3.text = infoCell.items?.key3
        self._lbValue3.text = infoCell.items?.value3
        
        
        self._lbKey1.font.withSize(FontSizes.FONT_TEXT)
        self._lbKey2.font.withSize(FontSizes.FONT_TEXT)
        self._lbKey3.font.withSize(FontSizes.FONT_TEXT)
        self._lbValue1.font.withSize(FontSizes.FONT_TEXT)
        self._lbValue2.font.withSize(FontSizes.FONT_TEXT)
        self._lbValue3.font.withSize(FontSizes.FONT_TEXT)
    }
    
    static var className: String {
        return String(describing: self)
    }
}
