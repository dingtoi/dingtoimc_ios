//
//  PhoneNumberView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/22/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhoneNumberView: BaseView {

    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _tfPhoneNumber: UITextField!
    
    var handler: ScannerHandler?
    
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popViewController()
    }
    
    @IBAction func pressNext(_ sender: Any) {
        if TelephonyStatus.shared.isSimCardAvailable {
            if let phoneNumber = self._tfPhoneNumber.text, phoneNumber != "" {
                self.checkTextInbound(phoneNumber: phoneNumber)
            } else {
                self.rootVC?.showDialog("Warning", "Phone number is required")
            }
        } else {
            self.pushViewController()
        }
    }
    
    override func setControlView() {
        self.handler?.dataSource = self
        self._lbTitle.font = self._lbTitle.font.withSize(UIDevice().autoSize(35, 40, 42, 46))
        self._lbTitle.text = "Input\nphone\nnumber"
        self._lbTitle.setTextSpacingBy(textSpace: 2, lineSpace: 3)
        self._tfPhoneNumber.frame.height = 50
        self._btnNext.layer.cornerRadius = self._btnNext.frame.height/2
        #if DEBUG
        self._tfPhoneNumber.text = "080667171"
        #endif
        if !TelephonyStatus.shared.isSimCardAvailable {
            let alertController = UIAlertController(title: "Warning", message:
                "Please insert SIM!", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))

            self.rootVC?.present(alertController, animated: true, completion: nil)
        }
    }

}

extension PhoneNumberView: ScannerProtocol {
    func scanner() {
        
    }
    
    func checkTextInbound(phoneNumber: String) {
        self.handler?.checkTextInbound(phoneNumber: phoneNumber)
    }
    
    func checkVoiceInbound(phoneNumber: String) {
        self.handler?.checkVoiceInbound(phoneNumber: phoneNumber)
    }
}

extension PhoneNumberView: ScannerDataSource {
    func errorInbound(error: ResponseErrorModel) {
        self.rootVC?.showDialog("Error", error.message ?? "")
    }
    
    func scanned(deviceInfo: DeviceInfo, data: Array<Array<InfoCell>>) {
    }
    
    func successTextInbound(response: ResponeModel) {
        self.handler?.checkVoiceInbound(phoneNumber: self._tfPhoneNumber.text ?? "")
    }
    
    func successVoiceInbound(response: ResponeModel) {
        self.pushViewController()
    }
    
    func pushViewController() {
        let scanViewController = ScannerViewController()
        scanViewController.handler = self.handler
        self.rootVC?.pushViewController(viewController: scanViewController)
    }
}
