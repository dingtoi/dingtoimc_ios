//
//  PhoneNumberViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/22/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhoneNumberViewController: BaseViewController {
    @IBOutlet weak var _view: PhoneNumberView! {
        didSet {
            baseView = _view
            self._view.rootVC = self
            self._view.handler = ScannerController(rootVC: self)
        }
    }
}
