//
//  ScreenView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class ScreenView: BaseView {

    var line = [CGPoint]()
    var isTouch = false
    var location = CGPoint(x: 0, y: 0)
    @IBOutlet weak var _imgTouch: UIImageView!
    @IBOutlet weak var _lbDescription: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBAction func pressNext(_ sender: Any) {
        self.line.removeAll()
        self.setNeedsDisplay()
        self.rootVC?.pushToViewController(viewController: PhoneNumberViewController.self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch! = touches.first! as UITouch
        location = touch.location(in: self)
        if location.x < self._imgTouch.frame.maxX && location.x > self._imgTouch.frame.minX && location.y > self._imgTouch.frame.minY && location.y < self._imgTouch.frame.maxY {
            self.isTouch = true
            self._lbDescription.isHidden = true
            self._imgTouch.isHidden = true
        }
        self._btnNext.isHidden = true
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.isTouch {
            let touch : UITouch! =  touches.first! as UITouch
            location = touch.location(in: self)
            self.line.append(location)
            self.setNeedsDisplay()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.isTouch {
            self._imgTouch.center = self.center
            self._btnNext.isHidden = false
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.setStrokeColor(UIColor.blue.cgColor)
        context.setLineWidth(self._imgTouch.frame.width)
        context.setLineCap(.butt)
        for (i, p) in self.line.enumerated() {
            if i == 0 {
                context.move(to: p)
            } else {
                context.addLine(to: p)
            }
        }
        
        context.strokePath()
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            self._imgTouch.center = self.center
            self._imgTouch.isHidden = false
            self.backgroundColor = .white
            self._lbDescription.isHidden = false
            self.isTouch = false
            self._btnNext.isHidden = true
        }
    }

}
