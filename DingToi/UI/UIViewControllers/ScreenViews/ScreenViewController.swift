//
//  ScreenViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/15/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class ScreenViewController: BaseViewController {
    @IBOutlet weak var _view: ScreenView! {
        didSet {
            isBottomSafeArea = false
            isTopSafeArea = false
            isStatusBarHidden = true
            baseView = _view
            _view.rootVC = self
        }
    }
    
}
