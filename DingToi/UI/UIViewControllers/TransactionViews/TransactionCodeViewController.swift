//
//  TransactionCodeViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class TransactionCodeViewController: BaseViewController {
    @IBOutlet weak var _view: InputCodeView! {
        didSet {
            baseView = _view
            self._view.rootVC = self
            self._view.handler = TransactionController(rootVC: self)
        }
    }
    
    override func viewWillPop() {
        super.viewWillPop()
        self._view = nil
        print(CFGetRetainCount(self))
    }
}
