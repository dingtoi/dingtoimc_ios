//
//  InputCode.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/1/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class InputCodeView: BaseView, TransactionProtocol {
    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _tfTransactionCode: UITextField!
    
    var handler: TransactionHandler?
    
    @IBAction func pressNext(_ sender: Any) {
        if let transactionCode = self._tfTransactionCode.text, transactionCode != "" {
            self.checkTransactionCode(transactionCode: transactionCode)
        } else {
            self.rootVC?.showDialog("Warning", "Transaction code is required")
        }
    }
    
    override func setControlView() {
        self.handler?.dataSource = self
        self._lbTitle.font = self._lbTitle.font.withSize(UIDevice().autoSize(35, 40, 42, 46))
        self._lbTitle.text = "Input\ntransaction\ncode"
        self._lbTitle.setTextSpacingBy(textSpace: 2, lineSpace: 3)
        self._tfTransactionCode.frame.height = 50
        self._btnNext.layer.cornerRadius = self._btnNext.frame.height/2
        #if DEBUG
        self._tfTransactionCode.text = "8nDWEVqPe"
        #endif
    }

    func checkTransactionCode(transactionCode: String) {
        self.handler?.checkTransactionCode(transactionCode: transactionCode)
    }
}

extension InputCodeView: TransactionDataSource {
    func didFinished(response: ResponeModel) {
        if (response.isSuccess) {
            Share.transactionCode = _tfTransactionCode.text
            self.rootVC?.pushToViewController(viewController: ScreenViewController.self)
        } else {
            self.rootVC?.showDialog("Error", "Incorrect transaction code")
        }
    }
    
    func didFailed(error: ResponseErrorModel) {
        self.setNeedsUpdateConstraints()
        self.rootVC?.showDialog("Error", error.error?.code ?? "")
    }
}
