//
//  StartView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class StartView: BaseView {
    
    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _lbDescription: UILabel!
    @IBOutlet weak var _btnSubmit: UIButton!

    @IBAction func pressSubmit(_ sender: Any) {
        LevelPoint.physical = 50
        LevelPoint.scannerDetail = 50
        self.rootVC?.pushToViewController(viewController: TransactionCodeViewController.self)
    }
    
    override func setControlView() {
        RequestAccess.shared.requestAccessCamera()
    }
    
    override func updateUI() {
        if Share.isAgain {
            self._lbTitle.text = "THANK YOU"
            self._btnSubmit.setTitle("SCAN AGAIN", for: .normal)
        } else {
            self._lbTitle.text = "WELCOME DINGTOI"
            self._btnSubmit.setTitle("START", for: .normal)
        }
        self._btnSubmit.titleLabel?.font.withSize(FontSizes.FONT_MENU_CELL_TITLE)
        self._btnSubmit.frame.midX = self.frame.midX
        self._btnSubmit.layer.cornerRadius = self._btnSubmit.frame.height/2
        self._lbTitle.setTextSpacingBy(textSpace: 5, lineSpace: nil)
    }

    override func willMove(toWindow newWindow: UIWindow?) {
        self.updateUI()
    }
}
