//
//  StartViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/1/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class StartViewController: BaseViewController {
    
    @IBOutlet weak var _view: BaseView! {
        didSet {
            baseView = _view
            self._view.rootVC = self
        }
    }
    
    override func viewWillPop() {
        super.viewWillPop()
        self._view = nil
        print(CFGetRetainCount(self))
    }
}
