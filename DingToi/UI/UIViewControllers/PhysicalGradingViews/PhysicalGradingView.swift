//
//  PhysicalGradingView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/13/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhysicalGradingView: BaseView {
    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _btnNext: UIButton!
    @IBOutlet weak var _tableView: UITableView!
    var indexPathSelected: IndexPath = IndexPath(row: 0, section: 0)
    var data: Array<PhysicalCellModel> = []
    
    @IBAction func pressNext(_ sender: Any) {
        self.rootVC?.pushToViewController(viewController: DiamondRatingViewController.self)
    }
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popViewController()
    }
    
    override func setControlView() {
        var rowSelect = 0
        switch LevelPoint.physical {
        case 50:
            rowSelect = 0
        case 40:
            rowSelect = 1
        case 30:
            rowSelect = 2
        default:
            rowSelect = 3
        }
        self.indexPathSelected.row = rowSelect
        self._lbTitle.text = "physical_grading.title".localized
        self._lbTitle.font = self._lbTitle.font.withSize(UIDevice().autoSize(22, 25, 25, 30))
        self._lbTitle.setTextSpacingBy(textSpace: 2, lineSpace: 3)
        initTableView()
    }
    
    func initTableView() {
        self.initData()
        self._tableView.delegate = self
        self._tableView.dataSource = self
        
        self._tableView.register(UINib(nibName: "PhysicalGradingCell", bundle: nil), forCellReuseIdentifier: "physicalGradingCell")
    }
    
    func initData() {
        self.data.removeAll()
        self.data.append(PhysicalCellModel(title: "physical_grading.grade_a.title".localized, value: "physical_grading.grade_a.description".localized, isCheck: LevelPoint.physical == 50))
        self.data.append(PhysicalCellModel(title: "physical_grading.grade_b.title".localized, value: "physical_grading.grade_b.description".localized, isCheck: LevelPoint.physical == 40))
        self.data.append(PhysicalCellModel(title: "physical_grading.grade_c.title".localized, value: "physical_grading.grade_c.description".localized, isCheck: LevelPoint.physical == 30))
        self.data.append(PhysicalCellModel(title: "physical_grading.grade_d.title".localized, value: "physical_grading.grade_d.description".localized, isCheck: LevelPoint.physical <= 10))
    }
    
}

extension PhysicalGradingView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 140
        case 1:
            return 100
        default:
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "physicalGradingCell") as! PhysicalGradingCell
        cell.loadData(data: self.data[indexPath.row])
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected \(indexPath.section) - \(indexPath.row)")
        switch indexPath.row {
        case 0:
            LevelPoint.physical = 50
        case 1:
            LevelPoint.physical = 40
        case 2:
            LevelPoint.physical = 30
        case 3:
            LevelPoint.physical = 10
        default:
            LevelPoint.physical = 0
        }
        let indexPathSelectedOld = self.indexPathSelected
        self.indexPathSelected = indexPath
        self.data[indexPathSelectedOld.row].isCheck = false
        self.data[indexPath.row].isCheck = true
        tableView.reloadRows(at: [indexPathSelectedOld, indexPath], with: .none)
    }
}
