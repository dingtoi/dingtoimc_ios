//
//  PhysicalGradingViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit

class PhysicalGradingViewController: BaseViewController {
    @IBOutlet weak var _view: PhysicalGradingView! {
        didSet {
            isBottomSafeArea = false
            baseView = _view
            _view.rootVC = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
