//
//  DiamondRatingView.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/14/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit
import Cosmos

class DiamondRatingView: BaseView {
    @IBOutlet weak var _lbTitle: UILabel!
    @IBOutlet weak var _lbDeviceName: UILabel!
    @IBOutlet weak var _lbTransactionCode: UILabel!
    @IBOutlet weak var _lbValueRate: UILabel!
    @IBOutlet weak var _viewSystemRate: CosmosView!
    @IBOutlet weak var _viewCustomRate: CosmosView!
    @IBOutlet weak var _btnSubmit: UIButton!
    @IBOutlet weak var _imgShadow: UIImageView!
    
    @IBAction func pressBack(_ sender: Any) {
        self.rootVC?.popViewController()
    }
    @IBAction func pressSubmit(_ sender: Any) {
        Share.isAgain = true
        Share.logString.append("15. Physical Grade Score: \(LevelPoint.physical)")
        if !Utils.shared.writeToFile(fileName: "Dingtoi.txt", data: "\(Share.logString)", atomically: false) {
            print("can't not create file")
        }
        self.rootVC?.pushToViewController(viewController: StartViewController.self)
    }
    
    override func setControlView() {
        self._lbTitle.text = "diamond_rating.title".localized
        self._lbTitle.font = self._lbTitle.font.withSize(UIDevice().autoSize(22, 25, 25, 30))
        self._lbTitle.setTextSpacingBy(textSpace: 2, lineSpace: 3)
        self._btnSubmit.roundedBottom(size: 10)
        self._lbTransactionCode.text = Share.transactionCode
        self._lbDeviceName.text = UIDevice.modelName
        let totalPoint = LevelPoint.physical + LevelPoint.scannerDetail
        print(totalPoint)
        if totalPoint < 60 {
            self._viewSystemRate.rating = 1
            self._lbValueRate.text = "1star".localized
        } else if totalPoint < 70 {
            self._viewSystemRate.rating = 2
            self._lbValueRate.text = "2star".localized
        } else if totalPoint < 80 {
            self._viewSystemRate.rating = 3
            self._lbValueRate.text = "3star".localized
        } else if totalPoint < 90 {
            self._viewSystemRate.rating = 4
            self._lbValueRate.text = "4star".localized
        } else {
            self._viewSystemRate.rating = 5
            self._lbValueRate.text = "5star".localized
        }
    }
}
