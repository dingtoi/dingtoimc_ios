//
//  DiamondRatingViewController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit
import Cosmos

class DiamondRatingViewController: BaseViewController {
    @IBOutlet weak var _view: DiamondRatingView! {
        didSet {
            baseView = _view
            _view.rootVC = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
