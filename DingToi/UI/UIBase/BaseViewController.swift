//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import UIKit
import DeviceKit
import ContactsUI
import Contacts
import CoreData
import CoreBluetooth
import Foundation

class BaseViewController: UIViewController {
    var isStatusBarHidden = false
    var isNavigationBarHidden = true
    var isTopSafeArea = true
    var isLeftSafeArea = true
    var isRightSafeArea = true
    var isBottomSafeArea = true
    var baseView: BaseView?
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = self.isNavigationBarHidden
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
        self.addConstraint()
        self.baseView?.setControlView()
        self.baseView?.applyFont()
        self.baseView?.adjustView()
        self.baseView?.setActionView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print(CFGetRetainCount(self))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return self.isStatusBarHidden
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    func viewWillPop() {
        print(CFGetRetainCount(self))
        self.baseView = nil
    }
    
    deinit {
        print("Base deinit called")
    }
    
    func addConstraint() {
        self.baseView?.translatesAutoresizingMaskIntoConstraints = true
        self.baseView?.frame = self.getFrame()
    }
    
    func getFrame() -> CGRect {
        let heightStatusBar = self.isStatusBarHidden ? 0 : UIApplication.shared.statusBarFrame.height
        var frame = CGRect()
        if let window = UIApplication.shared.keyWindow {
            if isLeftSafeArea, #available(iOS 11.0, *) {
                frame.x = window.safeAreaInsets.left
            } else {
                frame.x = window.frame.left
            }
            
            if isTopSafeArea, #available(iOS 11.0, *) {
                if window.safeAreaInsets.top == 0 {
                    frame.y = window.safeAreaInsets.top + heightStatusBar
                } else {
                    frame.y = window.safeAreaInsets.top
                }
            } else {
                frame.y = window.frame.top + heightStatusBar
            }
            
            if isRightSafeArea, #available(iOS 11.0, *) {
                frame.width = UIScreen.main.bounds.width - window.safeAreaInsets.right - frame.x
            } else {
                frame.width = window.frame.right - frame.x
            }
            
            if isBottomSafeArea, #available(iOS 11.0, *) {
                frame.height = UIScreen.main.bounds.height - window.safeAreaInsets.bottom - frame.y
            } else {
                frame.height = window.frame.bottom - frame.y
            }
        }
        return frame
    }
    
    private var loadingView: LoadingView = LoadingView(frame: CGRect(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
    private var loadingProgressView: LoadingProgressView = LoadingProgressView.init(frame: UIScreen.main.bounds)
    private var loadingProgressCancelView: LoadingProgressCancelView = LoadingProgressCancelView.init(frame: UIScreen.main.bounds)
    
    //Progress
    public func showProgress(){
        loadingView.tag = 999999
        self.view.addSubview(loadingView)
    }
    
    public func hideProgress(){
        if let viewWithTag = self.view.viewWithTag(999999) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress ./
    
    //Progress Dialog
    public func showProgressDialog(value: String){
        loadingProgressView.tag = 888888
        loadingProgressView.updateStatus(value: value)
        self.view.addSubview(loadingProgressView)
    }
    
    public func updateProgressDialog(value: String){
        if let viewWithTag = self.view.viewWithTag(888888) {
            (viewWithTag as? LoadingProgressView)?.updateStatus(value: value)
        }
    }
    
    public func checkProgressDialog() -> Bool{
        if self.view.viewWithTag(888888) != nil {
            return true
        }
        return false
    }
    
    public func hideProgressDialog(){
        if let viewWithTag = self.view.viewWithTag(888888) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress Dialog ./
    
    //Progress Cancel
    public func showProgressCancel(value: String){
        loadingProgressCancelView.tag = 777777
        loadingProgressCancelView.updateStatus(value: value)
        self.view.addSubview(loadingProgressCancelView)
    }
    
    public func updateProgressCancel(value: String){
        if let viewWithTag = self.view.viewWithTag(777777) {
            (viewWithTag as? LoadingProgressCancelView)?.updateStatus(value: value)
        }
    }
    
    public func checkProgressCancel() -> Bool{
        if self.view.viewWithTag(777777) != nil {
            return true
        }
        return false
    }
    
    public func hideProgressCancel(){
        if let viewWithTag = self.view.viewWithTag(777777) {
            viewWithTag.removeFromSuperview()
        }
    }
    //Progress Cancel ./
    
    
    func showDialog(_ title: String,_ message: String) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15),
            NSAttributedStringKey.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black // Button
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkIphoneX() -> Bool {
        if Device.current.isOneOf(UIDevice.groupOfLargeX){
            return true
        }
        return false
    }
    
    
    func pushToViewController(viewController: NSObject.Type) {
        print("pushToViewController \(viewController)")
        let tmp =  self.navigationController?.viewControllers.filter({type(of: $0) == viewController}).first
        if let vc = tmp {
            self.navigationController?.popToViewController(vc, animated: false)
        } else {
            self.navigationController?.pushViewController(viewController.init() as! BaseViewController, animated: false)
        }
    }
    
    public func pushViewController(viewController: BaseViewController) {
        print("push viewController \(type(of: viewController))")
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func popViewController() {
        print("popViewController \(type(of: self))")
        self.navigationController?.popViewController(animated: false)
    }
}
