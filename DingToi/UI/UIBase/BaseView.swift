//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class BaseView: UIView, UITextFieldDelegate {
    
    var rootVC: BaseViewController?
    
    //Called when the user click on the view (outside the UITextField).
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    //Called when 'return' key pressed. return NO to ignore.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Show keyboard move layout with position viewcontroller --> frameCurrentFocus
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.getRootViewController()?.frameCurrentFocus = textField //get frame when focous textField
    }
    
    func className() -> String {
        return String(describing: type(of: self)) // NOTES: --> xib Name from self
    }
    
    func xibSetup(frame: CGRect) {
        let view = UINib(nibName: className(), bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = frame
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setControlView()
        applyFont()
        adjustView()
        setActionView()
    }
    
    func setControlView() {}
    func setActionView() {}
    func adjustView() {}
    func applyFont() {}
    func updateUI() {}
    func updateUIEdit() {}
    
    func getRootViewController() -> BaseViewController? {
        return self.rootVC
    }
    
    func showMessage(_ message: String) {
        self.getRootViewController()?.showDialog(String.kErrorMessageTitle, message)
    }
    
    private var loadingView: LoadingView = LoadingView(frame: CGRect(origin: .zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)))
    public func showProgressOnView(){
        loadingView.tag = 999989
        addSubview(loadingView)
    }
    
    public func hideProgressOnView(){
        if let viewWithTag = viewWithTag(999989) {
            viewWithTag.removeFromSuperview()
        }
    }
}
