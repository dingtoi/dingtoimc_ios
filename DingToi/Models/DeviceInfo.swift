//
//  DeviceInfo.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
class DeviceInfo: NSObject {
    var deviceName: String
    
    init(deviceName: String) {
        self.deviceName = deviceName
    }
}
