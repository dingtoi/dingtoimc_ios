//
//  PhysicalCellModel.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/14/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
class PhysicalCellModel: NSObject {
    var title: String
    var value: String
    var isCheck: Bool
    
    init(title: String, value: String, isCheck: Bool) {
        self.title = title
        self.value = value
        self.isCheck = isCheck
    }
}
