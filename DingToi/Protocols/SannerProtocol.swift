//
//  SannerProtocol.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
protocol ScannerProtocol {
    func scanner()
    func checkTextInbound(phoneNumber: String)
    func checkVoiceInbound(phoneNumber: String)
}

protocol ScannerHandler: ScannerProtocol {
    var delegate: ScannerDelegate? { get set }
    var dataSource: ScannerDataSource? { get set }
    
    func addLog(data: Array<Array<InfoCell>>)
}

protocol ScannerDelegate {
    
}

protocol ScannerDataSource {
    func scanned(deviceInfo: DeviceInfo, data: Array<Array<InfoCell>>)
    func successTextInbound(response: ResponeModel)
    func successVoiceInbound(response: ResponeModel)
    func errorInbound(error: ResponseErrorModel)
}
