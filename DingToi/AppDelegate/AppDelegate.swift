//  Created by boys vip on 5/23/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import DeviceKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    // iOS9, called when presenting notification in background
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if UIApplication.shared.applicationState == .active {
            print("==> Handle background notification")
        }else {
            print("==> Handle foreground notification")
        }
    }
    
    func pushToStartViewController() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let startVC = StartViewController()
        self.window?.rootViewController = UINavigationController(rootViewController: startVC)
        self.window?.makeKeyAndVisible()
    }
    
    var launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("==> Welcome")
        FirebaseApp.configure()
        self.pushToStartViewController()
        
        self.launchOptions = launchOptions
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //alarmScheduler.checkNotification() //Only true on IOS 8.0 - 9.0
    }
}
