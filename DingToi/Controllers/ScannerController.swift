//
//  ScannerController.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/3/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit
import DeviceKit
import CoreBluetooth
import AVFoundation

class ScannerController: ScannerHandler {
    var rootVC: BaseViewController?
    var session: AVCaptureSession?
    var input: AVCaptureDeviceInput?
    var delegate: ScannerDelegate?
    var dataSource: ScannerDataSource?
    var isTextInbound: Bool?
    var isVoiceInbound: Bool?
    
    init(rootVC: BaseViewController) {
        self.rootVC = rootVC
    }
    
    func scanner() {
        LevelPoint.scannerDetail = 50
        Share.logString = ""
        self.dataSource?.scanned(deviceInfo: self.deviceInfo(), data: self.data())
    }
    
    func checkTextInbound(phoneNumber: String) {
        self.rootVC?.showProgress()
        APIManager.shared.checkTextInbound(phoneNumber: phoneNumber, delegate: self)
    }
    
    func checkVoiceInbound(phoneNumber: String) {
        self.rootVC?.showProgress()
        APIManager.shared.checkVoiceInbound(phoneNumber: phoneNumber, delegate: self)
    }
    
    func data() -> Array<Array<InfoCell>> {
        var infos = Array<InfoCell>()
        
        // HARDWARE
        let value3 = "\(DiskStatus.usedDiskSpace() ?? "N/A") of \(DiskStatus.totalDiskSpace() ?? "N/A") used"
        let harewareItem = Items(key1: "scanner_detail.hardware.title1".localized, value1: UIDevice.current.systemVersion, key2: "scanner_detail.hardware.title2".localized, value2: UIDevice.current.getCPUName(), key3: "scanner_detail.hardware.title3".localized, value3: value3)
        infos.append(InfoCell(key: "scanner_detail.hardware.title".localized, value: "", imageName: "icon_hardware", isFailed: false, items: harewareItem))
        
        
        // CAMERA + FLASH
        let isCameraAvailable = self.checkCameraStatus()
        let isFlashAvailable = self.checkFlashStatus()
        if !isCameraAvailable {
            LevelPoint.scannerDetail -= 10
        }
        if !isFlashAvailable {
            LevelPoint.scannerDetail -= 5
        }
        let cameraItem = Items(key1: "Camera", value1: "\(isCameraAvailable)", key2: "Flash", value2: "\(isFlashAvailable)", key3: nil, value3: nil)
        let isWorking = isCameraAvailable && isFlashAvailable
        infos.append(InfoCell(key: "Camera + Flash", value: "Camera + Flash \(isWorking ? "working" : "not working")", imageName: "icon_camera", isFailed: !isWorking, items: cameraItem))
        
        // VOLUME + SPEAKER
        let isVolumeAvailable = self.checkVolumeControl()
        let isSpeakerAvailable = self.checkSpeaker()
        if !isVolumeAvailable {
            LevelPoint.scannerDetail -= 5
        }
        if !isSpeakerAvailable {
            LevelPoint.scannerDetail -= 5
        }
        let volumeItem = Items(key1: "Volume", value1: "\(isVolumeAvailable)", key2: "Speaker", value2: "\(isSpeakerAvailable)", key3: nil, value3: nil)
        infos.append(InfoCell(key: "Volume adjustment", value: nil, imageName: "icon_volume", isFailed: !(isVolumeAvailable && isSpeakerAvailable), items: volumeItem))
        
        
        var wifi = Array<InfoCell>()
        
        // WIFI
        if !NetworkStatus.shared.status() {
            LevelPoint.scannerDetail -= 10
        }
        wifi.append(InfoCell(key: "Wifi", value: "Wifi \(NetworkStatus.shared.status() ? "working" : "not working")", imageName: "icon_wifi", isFailed: !NetworkStatus.shared.status(), items: nil))
        
        // BLUETOOTH
        wifi.append(InfoCell(key: "Bluetooth", value: nil, imageName: "icon_bluetooth", isFailed: nil, items: nil))
        
        // FINGER SCAN
        if !FingerprintStatus.shared.status() {
            LevelPoint.scannerDetail -= 2
        }
        wifi.append(InfoCell(key: "Finger Scanner", value: "Finger Scanner \(FingerprintStatus.shared.status() ? "working" : "not working")", imageName: "icon_fingerprint", isFailed: !FingerprintStatus.shared.status(), items: nil))
        
        
        var advance = Array<InfoCell>()
        
        // VOICE
        let voiceCallItem = Items(key1: "Outbound", value1: "\(TelephonyStatus.shared.isCall)", key2: "Inbound", value2: "\(self.isVoiceInbound ?? false)", key3: nil, value3: nil)
        advance.append(InfoCell(key: "Voice calls", value: nil, imageName: "icon_call", isFailed: !TelephonyStatus.shared.isCall, items: voiceCallItem))
        
        // SMS
        let textingItem = Items(key1: "Outbound", value1: "\(TelephonyStatus.shared.isSMS)", key2: "Inbound", value2: "\(self.isTextInbound ?? false)", key3: nil, value3: nil)
        advance.append(InfoCell(key: "Texting", value: nil, imageName: "icon_sms", isFailed: TelephonyStatus.shared.isSMS, items: textingItem))
        
        return [infos, wifi, advance]
    }
    
    func deviceInfo() -> DeviceInfo {
        return DeviceInfo(deviceName: UIDevice.modelName)
    }
    
    func checkVolumeControl() -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            return true
        }  catch {
           return false
       }
    }
    
    func checkSpeaker() -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            let currentRoute = audioSession.currentRoute
            if currentRoute.outputs.count == 0 {
                return false
            } else {
                for output in currentRoute.outputs {
                    print("\(output.portName) - \(output.portType)")
                }
            }
            return true
        } catch {
            return false
        }
    }
}

extension ScannerController {
    
    func checkFlashStatus() -> Bool {
        if let device = self.getDevice(position: .back) {
            return flashOn(device: device)
        }
        return false
    }
    
    func checkCameraStatus() -> Bool {
        self.session = AVCaptureSession()
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if UIImagePickerController.isCameraDeviceAvailable(.rear), authStatus != .authorized {
            return false
        }
        if let camera = self.getDevice(position: .back) {
            do {
                input = try AVCaptureDeviceInput(device: camera)
            } catch let error as NSError {
                print("==> Start camera error:")
                print(error)
                return false
            }
            
            if let input = self.input, session!.canAddInput(input) {
                print("==> Camera working")
                // Device working
                return true
            } else {
                print("==> Check camera: Can not use camera")
                // Can not use camera
            }
        } else {
            print("==> Check camera: Device not support camera")
            // Device not support camera
        }
        return false
    }
    
    //Get the device (Front or Back)
    func getDevice(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
               return deviceConverted
            }
        }
       return nil
    }
    
    func flashOn(device:AVCaptureDevice) -> Bool {
        do {
            if (device.hasTorch) {
                try device.lockForConfiguration()
                device.torchMode = .on
                device.flashMode = .on
                device.torchMode = .off
                device.flashMode = .off
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
            return false
        }
        return true
    }
    
    func addLog(data: Array<Array<InfoCell>>) {
        var i = 1
        for cell in data {
            for infoCell in cell {
                if let items = infoCell.items {
                    if let value1 = items.value1, let key1 = items.key1 {
                        if value1 == "true" || value1 == "false" {
                            var key = ""
                            if key1 == "Inbound" || key1 == "Outbound" {
                                key = "\(infoCell.key!) \(key1)"
                            } else {
                                key = key1
                            }
                            Share.logString.append("\(i). \(key): \(value1 == "true" ? "working" : "not working")\n")
                        } else {
                            Share.logString.append("\(i). \(key1): \(value1)\n")
                        }
                        i += 1
                    }
                    if let value2 = items.value2, let key2 = items.key2 {
                        if value2 == "true" || value2 == "false" {
                            var key = ""
                            if key2 == "Inbound" || key2 == "Outbound" {
                                key = "\(infoCell.key!) \(key2)"
                            } else {
                                key = key2
                            }
                            Share.logString.append("\(i). \(key): \(value2 == "true" ? "working" : "not working")\n")
                        } else {
                            Share.logString.append("\(i). \(key2): \(value2)\n")
                        }
                        i += 1
                    }
                    if let value3 = items.value3, let key3 = items.key3 {
                        if value3 == "true" || value3 == "false" {
                            Share.logString.append("\(i). \(key3): \(value3 == "true" ? "working" : "not working")\n")
                        } else {
                            Share.logString.append("\(i). \(key3): \(value3)\n")
                        }
                        i += 1
                    }
                } else {
                    if let value = infoCell.value, let key = infoCell.key {
                        Share.logString.append("\(i). \(value.replacingOccurrences(of: key, with: "\(key):"))\n")
                        i += 1
                    }
                }
            }
        }
    }
}

extension ScannerController: InboundDelegate {
    func onSuccessTextInbound(response: ResponeModel) {
        self.rootVC?.hideProgress()
        self.isTextInbound = response.isSuccess
        self.dataSource?.successTextInbound(response: response)
    }
    
    func onSuccessVoiceInbound(response: ResponeModel) {
        self.rootVC?.hideProgress()
        self.isVoiceInbound = response.isSuccess
        self.dataSource?.successVoiceInbound(response: response)
    }
    
    func onErrorInbound(error: ResponseErrorModel) {
        self.rootVC?.hideProgress()
        self.dataSource?.errorInbound(error: error)
    }
    
}
