//
//  DiskStatus.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/29/20.
//  Copyright © 2020 boys vip. All rights reserved.
//

import UIKit

let MB = 1024 * 1024
let GB = MB * 1024

class DiskStatus {
// MARK: - Formatter
    class func memoryFormatter(_ diskSpace: Int64) -> String? {
        var formatted: String?
        let bytes = 1.0 * Double(diskSpace)
        let megabytes = bytes / Double(MB)
        let gigabytes = bytes / Double(GB)
        if gigabytes >= 1.0 {
            formatted = String(format: "%.2f GB", gigabytes)
        } else if megabytes >= 1.0 {
            formatted = String(format: "%.2f MB", megabytes)
        } else {
            formatted = String(format: "%.2f bytes", bytes)
        }

        return formatted
    }

// MARK: - Methods
    class func totalDiskSpace() -> String? {
        var space: Int64? = nil
        do {
            space = (try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())[.systemSize] as? NSNumber)?.int64Value ?? 0
        } catch {
        }
        
        var total: Int = 0
        
        if space! > Int64(0) && space! < Int64(17179869184) {
            space = 17179869184
            total = 16
        } else if space! < Int64(34359738368) {
            space = 34359738368
            total = 32
        } else if space! < Int64(68719476736) {
            space = 68719476736
            total = 64
        } else if space! < Int64(137438953472) {
            space = 137438953472
            total = 128
        } else if space! < Int64(274877906944) {
            space = 274877906944
            total = 256
        } else if space! < Int64(549755813888) {
            space = 549755813888
            total = 512
        } else if space! < Int64(1099511627776) {
            space = 1099511627776
            total = 1024
        } else {
            
        }
        return String(format: "%d GB", total)
    }

    class func freeDiskSpace() -> String? {
        var freeSpace: Int64? = nil
        do {
            freeSpace = (try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())[.systemFreeSize] as? NSNumber)?.int64Value ?? 0
        } catch {
        }
        return self.memoryFormatter(freeSpace ?? 0)
    }

    class func usedDiskSpace() -> String? {
        return self.memoryFormatter(Int64(self.usedDiskSpaceInBytes()))
    }

    class func totalDiskSpaceInBytes() -> CGFloat {
        var space: Int64? = nil
        do {
            space = (try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())[.systemSize] as? NSNumber)?.int64Value ?? 0
        } catch {
        }
        return CGFloat(space ?? 0)
    }

    class func freeDiskSpaceInBytes() -> CGFloat {
        var freeSpace: Int64? = nil
        do {
            freeSpace = (try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())[.systemFreeSize] as? NSNumber)?.int64Value ?? 0
        } catch {
        }
        return CGFloat(freeSpace ?? 0)
    }

    class func usedDiskSpaceInBytes() -> CGFloat {
        let usedSpace = Int64(self.totalDiskSpaceInBytes() - self.freeDiskSpaceInBytes())
        return CGFloat(usedSpace)
    }

    /// WORK IN PROGRESS
    class func numberOfNodes() -> CGFloat {
        var numberOfNodes: Int64? = nil
        do {
            numberOfNodes = (try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory())[.systemNodes] as? NSNumber)?.int64Value ?? 0
        } catch {
        }
        return CGFloat(numberOfNodes ?? 0)
    }
}
