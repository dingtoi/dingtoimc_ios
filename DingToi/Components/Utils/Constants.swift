//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import Foundation
import DeviceKit

public struct Constants {
    public static let TAG_MENU_VIEW:Int = 111
}

public struct Fonts {
    public static var FONT_TEXT_IN_LB_AND_TXT_OF_INFOR: CGFloat {
        return UIDevice().autoSize(12, 14, 16, 16)
    }
    
    public static var FONT_TEXT_CELL_MAP: CGFloat {
        return UIDevice().autoSize(14, 16, 18, 18)
    }
}

public struct FunctionName {
    
    // Section 0
    public static var HARDWARE = 0
    public static var CAMERA = 1
    public static var VOLUME = 2
    
    // Section 1
    public static var WIFI = 0
    public static var BLUETOOTH = 1
    public static var FINGERPRINTER = 2
    
    // Section 2
    public static var VOICE_CALL = 0
    public static var TEXTING = 1
    public static var DATA_TRANSFER = 2
    public static var SPECIAL_APP = 3
}
