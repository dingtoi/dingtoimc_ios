//
//  CallsStatus.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import CoreTelephony
import UIKit
import MessageUI

class TelephonyStatus {
    public static let shared = TelephonyStatus()
    
    var isCall: Bool {
        let isCapableToCall: Bool
        if UIApplication.shared.canOpenURL(NSURL(string: "tel://")! as URL) {
            // Check if iOS Device supports phone calls
            // User will get an alert error when they will try to make a phone call in airplane mode
            if let mnc = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode, !mnc.isEmpty {
                // iOS Device is capable for making calls
                isCapableToCall = true
            } else {
                // Device cannot place a call at this time. SIM might be removed
                isCapableToCall = false
            }
        } else {
            // iOS Device is not capable for making calls
            isCapableToCall = false
        }
        
        return isCapableToCall
    }
    
    var isSMS: Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    var isSimCardAvailable: Bool {

        let info = CTTelephonyNetworkInfo()
        if let carrier = info.subscriberCellularProvider {
            if let code = carrier.mobileNetworkCode {
                if !code.isEmpty {
                    return true
                }
            }
        }
        return false
    }
}
