//
//  Utilities.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/18/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
class Utils: NSObject {
    static let shared = Utils()
        
    // MARK: Methods for get/set account info to NSUserDefault
    func setDataWithKey(objectData:AnyObject?, withKey:String!) {
        UserDefaults.standard.set(objectData, forKey: withKey)
    }
        
    func getDataWithKey(key:String!) -> AnyObject! {
        return UserDefaults.standard.object(forKey: key) as AnyObject
    }
        
    func randomString(length: Int) -> String {
        
        let letters : NSString = "0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func writeToFile(fileName: String, data: String, encoding: String.Encoding = .utf8, atomically: Bool) -> Bool {
        let fileManager = FileManager.default
        if let dir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(fileName)
            print("====> Path: \(fileURL.path)")
            if(!fileManager.fileExists(atPath: fileURL.path)){
                fileManager.createFile(atPath: fileURL.path, contents: nil, attributes: nil)
            }else{
                print("File is already created")
            }
            do {
                try data.write(toFile: fileURL.path, atomically: atomically, encoding: encoding)
//                return true
            } catch let error {
                print(error.localizedDescription)
                return false
            }
            
            //reading
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                print(text2)
            } catch let error {
                print(error.localizedDescription)
                return false
            }
            return true
        } else {
            return false
        }
        
    }
}

extension Utils {
    func getLanguage() -> String! {
        self.setDataWithKey(objectData: "VI" as AnyObject, withKey: Constants.dataKeys.languageKey)
        return self.getDataWithKey(key: Constants.dataKeys.languageKey) as? String
    }

    func getCurrentLocale() -> String {
        var locale = "en"
        if let currentLocale = LocalPersistent.shared.getLanguage() {
            locale = currentLocale == "VI" ? "vi" : "en"
        }
        return locale
    }
}

extension Constants {
    struct dataKeys {
        static let languageKey = "Language"
    }
}
