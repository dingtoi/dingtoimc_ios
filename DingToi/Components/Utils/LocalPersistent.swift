//
//  LocalPersistent.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/18/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
class LocalPersistent: NSObject {
    struct DataKeys {
        static let language = "Language"
    }
    
    static let shared = LocalPersistent()
    
    func getLanguage() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: DataKeys.language)
    }
}
