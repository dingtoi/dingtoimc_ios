//
//  BatteryInfo.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 3/31/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit

class BatteryInfo: NSObject {
    public static let shared = BatteryInfo()
    
    func batteryLevel() -> String {
        let device = UIDevice.current;
        device.isBatteryMonitoringEnabled = true
        var batteryLevel: Float = 0.0;
        let batteryCharge = device.batteryLevel
        if (batteryCharge > 0) {
            batteryLevel = batteryCharge * 100.0;
        }
        return "\(Int(batteryLevel)) %"
    }
    
    func batteryInfo() {
        guard case let batteryCenterHandle = dlopen("/System/Library/PrivateFrameworks/BatteryCenter.framework/BatteryCenter", RTLD_LAZY), batteryCenterHandle != nil else {
            
            fatalError("BatteryCenter not found")
        }

        guard let c = NSClassFromString("BCBatteryDeviceController") as? NSObjectProtocol else {
            fatalError("BCBatteryDeviceController not found")
        }

        
        let instance = c.perform(Selector("sharedInstance")).takeUnretainedValue()

        if let devices = instance.value(forKey: "connectedDevices") as? [AnyObject] {
            
            // You will have more than one battery in connectedDevices if your device is using a Smart Case
            for battery in devices {
                print(battery)
            }
        }
    }
}

