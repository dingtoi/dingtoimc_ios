//
//  RequestAccess.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/17/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import AVFoundation
class RequestAccess: NSObject {
    public static var shared = RequestAccess()
    
    func requestAccessCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus != .authorized {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    print("granted")
                } else {
                    print("denied")
                }
            })
        }
    }
}
