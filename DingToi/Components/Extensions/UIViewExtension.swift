//
//  UIViewExtension.swift
//  DingToi
//
//  Created by Tran Nghia Hiep on 4/16/20.
//  Copyright © 2020 Alatka Solutions. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func roundedBottom(size: CGFloat){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight , .bottomLeft],
                                     cornerRadii: CGSize(width: size, height: size))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
//
//    var safeTopAnchor: NSLayoutYAxisAnchor {
//      if #available(iOS 11.0, *) {
//        return self.safeAreaLayoutGuide.topAnchor
//      }
//      return self.topAnchor
//    }
//
//    var safeLeftAnchor: NSLayoutXAxisAnchor {
//      if #available(iOS 11.0, *){
//        return self.safeAreaLayoutGuide.leftAnchor
//      }
//      return self.leftAnchor
//    }
//
//    var safeRightAnchor: NSLayoutXAxisAnchor {
//      if #available(iOS 11.0, *){
//        return self.safeAreaLayoutGuide.rightAnchor
//      }
//      return self.rightAnchor
//    }
//
//    var safeBottomAnchor: NSLayoutYAxisAnchor {
//      if #available(iOS 11.0, *) {
//        return self.safeAreaLayoutGuide.bottomAnchor
//      }
//      return self.bottomAnchor
//    }
}
