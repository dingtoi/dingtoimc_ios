//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import UIKit

extension UILabel {
    func setTextSpacingBy(textSpace: Double, lineSpace: CGFloat?) {
      if let textString = self.text {
        let attributedString = NSMutableAttributedString(string: textString)
        if let lineSpace = lineSpace {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = lineSpace
    //        style.minimumLineHeight = lin // change line spacing between each line like 30 or 40
            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: textString.count))
        }
        attributedString.addAttribute(NSAttributedStringKey.kern, value: textSpace, range: NSMakeRange(0, textString.count))
        attributedText = attributedString
      }
    }
}
