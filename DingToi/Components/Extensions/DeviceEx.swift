//  Created by boys vip on 11/17/18.
//  Copyright © 2018 boys vip. All rights reserved.
//


import Foundation
import DeviceKit

extension UIDevice {
    
    public var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    public enum ScreenType: String {
        case iPhone4 = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X, Xs"
        case iPhoneXs = "iPhone Xs Max"
        case iPhoneXr = "iPhone Xr"
        case unknown
    }
    
    public var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        case 2688:
            return .iPhoneXs
        case 1792:
            return .iPhoneXr
        default:
            return .unknown
        }
    }
    
    public static let groupOfSmall: [Device] =
        [.iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE,
         .simulator(.iPhone5), .simulator(.iPhone5c), .simulator(.iPhone5s), .simulator(.iPhoneSE)]
    public static let groupOfMedium: [Device] =
        [.iPhone6, .iPhone6s, .iPhone7, .iPhone8,
         .simulator(.iPhone6), .simulator(.iPhone6s), .simulator(.iPhone7), .simulator(.iPhone8)]
    public static let groupOfLarge: [Device] =
        [.iPhone6Plus,.iPhone6sPlus, .iPhone7Plus, .iPhone8Plus,
         .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus), .simulator(.iPhone8Plus)]
    public static let groupOfLargeX: [Device] =
        [.iPhoneX, .iPhoneXS, .iPhoneXSMax, .iPhoneXR,
         .iPhone11 , .iPhone11Pro,.iPhone11ProMax,
         .simulator(.iPhoneX), .simulator(.iPhoneXS), .simulator(.iPhoneXSMax), .simulator(.iPhoneXR),
         .simulator(.iPhone11), .simulator(.iPhone11Pro), .simulator(.iPhone11ProMax)]
    
    public func autoSize(_ small: CGFloat, _ medium: CGFloat, _ large: CGFloat, _ largeX: CGFloat) -> CGFloat {
        if Device.current.isOneOf(UIDevice.groupOfSmall) {
            return small
        } else if Device.current.isOneOf(UIDevice.groupOfMedium) {
            return medium
        }else if Device.current.isOneOf(UIDevice.groupOfLarge) {
            return large
        }else if Device.current.isOneOf(UIDevice.groupOfLargeX) {
            return largeX
        }
        return medium
    }
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()
    
    //Original Author: HAS
    // https://stackoverflow.com/questions/26028918/how-to-determine-the-current-iphone-device-model
    // Modified by Sam Trent
    
    /**********************************************
    *  getCPUName():
    *     Returns a hardcoded value of the current
    * devices CPU name.
    ***********************************************/
    public func getCPUName() -> String
    {
        let processorNames = Array(CPUinfo().values)
        return processorNames[0]
    }

    /**********************************************
    *  getCPUSpeed():
    *     Returns a hardcoded value of the current
    * devices CPU speed as specified by Apple.
    ***********************************************/
//    public func getCPUSpeed() -> String
//    {
//        let processorSpeed = Array(CPUinfo().values)
//        return processorSpeed[0]
//    }
    
    /**********************************************
    *  CPUinfo:
    *     Returns a dictionary of the name of the
    *  current devices processor and speed.
    ***********************************************/
    private func CPUinfo() -> Dictionary<String, String> {

        #if targetEnvironment(simulator)
            let identifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"]!
        #else

            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            let identifier = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8 , value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
        #endif

        switch identifier {
        case "iPod5,1":                                 return ["iPod touch (5th generation)":"A5"]
        case "iPod7,1":                                 return ["iPod touch (6th generation)":"A8"]
        case "iPod9,1":                                 return ["iPod touch (7th generation)":"A10 Fusion"]
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return ["iPhone 4":"A4"]
        case "iPhone4,1":                               return ["iPhone 4s":"A5"]
        case "iPhone5,1", "iPhone5,2":                  return ["iPhone 5":"A6"]
        case "iPhone5,3", "iPhone5,4":                  return ["iPhone 5c":"A6"]
        case "iPhone6,1", "iPhone6,2":                  return ["iPhone 5s":"A7"]
        case "iPhone7,2":                               return ["iPhone 6":"A8"]
        case "iPhone7,1":                               return ["iPhone 6 Plus":"A8"]
        case "iPhone8,1":                               return ["iPhone 6s":"A9"]
        case "iPhone8,2":                               return ["iPhone 6s Plus":"A9"]
        case "iPhone9,1", "iPhone9,3":                  return ["iPhone 7":"A10 Fusion"]
        case "iPhone9,2", "iPhone9,4":                  return ["iPhone 7 Plus":"A10 Fusion"]
        case "iPhone8,4":                               return ["iPhone SE":"A9"]
        case "iPhone10,1", "iPhone10,4":                return ["iPhone 8":"A11 Bionic"]
        case "iPhone10,2", "iPhone10,5":                return ["iPhone 8 Plus":"A11 Bionic"]
        case "iPhone10,3", "iPhone10,6":                return ["iPhone X":"A11 Bionic"]
        case "iPhone11,2":                              return ["iPhone XS":"A12 Bionic"]
        case "iPhone11,4", "iPhone11,6":                return ["iPhone XS Max":"A12 Bionic"]
        case "iPhone11,8":                              return ["iPhone XR":"A12 Bionic"]
        case "iPhone12,1":                              return ["iPhone 11":"A13 Bionic"]
        case "iPhone12,3":                              return ["iPhone 11 Pro":"A13 Bionic"]
        case "iPhone12,5":                              return ["iPhone 11 Pro Max":"A13 Bionic"]
        case "iPad1,1":                                 return ["iPad":"A4"]
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return ["iPad 2":"A5"]
        case "iPad3,1", "iPad3,2", "iPad3,3":           return ["iPad (3rd generation)":"A5X"]
        case "iPad3,4", "iPad3,5", "iPad3,6":           return ["iPad (4th generation)":"A6X"]
        case "iPad6,11", "iPad6,12":                    return ["iPad (5th generation)":"A9"]
        case "iPad7,5", "iPad7,6":                      return ["iPad (6th generation)":"A10"]
        case "iPad7,11", "iPad7,12":                    return ["iPad (7th generation)":"A10"]
        case "iPad4,1", "iPad4,2", "iPad4,3":           return ["iPad Air":"A7 Rev A"]
        case "iPad5,3", "iPad5,4":                      return ["iPad Air 2":"A8X"]
        case "iPad11,3", "iPad11,4":                    return ["iPad Air (3rd generation)":"A12 Bionic"]
        case "iPad2,5", "iPad2,6", "iPad2,7":           return ["iPad mini":"A5"]
        case "iPad4,4", "iPad4,5", "iPad4,6":           return ["iPad mini 2":"A7"]
        case "iPad4,7", "iPad4,8", "iPad4,9":           return ["iPad mini 3":"A7"]
        case "iPad5,1", "iPad5,2":                      return ["iPad mini 4":"A8"]
        case "iPad11,1", "iPad11,2":                    return ["iPad mini (5th generation)":"A12 Bionic"]
        case "iPad6,3", "iPad6,4":                      return ["iPad Pro (9.7-inch)":"A9X"]
        case "iPad6,7", "iPad6,8":                      return ["iPad Pro (12.9-inch)":"A9X"]
        case "iPad7,1", "iPad7,2":                      return ["iPad Pro (12.9-inch) (2nd generation)":"A10X"]
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return ["iPad Pro (12.9-inch) (3rd generation)":"A12Z"]
        case "iPad8,11", "iPad8,12":                    return ["iPad Pro (12.9-inch) (4th generation)":"A12Z"]
        case "iPad7,3", "iPad7,4":                      return ["iPad Pro (10.5-inch)":"A10X"]
        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return ["iPad Pro (11-inch)":"A12Z"]
        case "iPad8,9", "iPad8,10":                     return ["iPad Pro (11-inch) (2nd generation)":"A12Z"]
//        case "AppleTV1,1":                              return ["Apple TV (1st generation)":"N/A"]
//        case "AppleTV2,1":                              return ["Apple TV (2nd generation)":"A4"]
//        case "AppleTV3,1", "AppleTV3,2":                return ["Apple TV (3rd generation)":"A5 Rev B"]
//        case "AppleTV5,3":                              return ["A8":"1.4 GHz"]
//        case "AppleTV6,2":                              return ["A10X Fusion":"2.34 GHz"]
//        case "AudioAccessory1,1":                       return ["A8":"1.4 GHz"] // clock speed is a guess
        default:                                        return ["N/A":"N/A"]
        }
    }
}

